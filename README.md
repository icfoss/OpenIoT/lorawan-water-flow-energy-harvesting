### LoRaWAN Water Flow Energy Harvesting - Air Quality Node
Harvesting energy plays an important role in increasing the efficiency and lifetime of IoT devices. Energy harvesting systems have some limitations such as unavailability of the energy source from which energy is supposed to be harvested, low amount of harvested energy, inefficiency of the harvesting system, etc. To overcome these limitations, some efforts have been done and new models for harvesting energy have been formed which are discussed in this review concerning the energy source of the harvest. Micro Water Turbine is used to harvest energy from the flow of water through pipelines. The harvested energy is stored into a Li-ion battery which is able to supply required amount of power to LoRaWAN Air Quality node based on BME680.

### Prerequisites
1. LoRaWAN & STM32 based Controller: https://gitlab.com/icfoss/OpenIoT/c1_dev_v1.0
2. Micro Water Turbine - Hydroelectric Generator (DC-5V) with G1/2'' Thread: www.seeedstudio.com/Micro-Water-Tubine-Generator-DC-5V-p-4512.html
3. Energy Harvester Generic Board for LPWAN: https://gitlab.com/soorajvs13/energy-harvester-generic-board-for-lpwan
4. 150mAh Battery: https://www.mouser.in/ProductDetail/Nichicon/SLB12400L1511CA?qs=DRkmTr78QARr5WxZbC3vqA%3D%3D
5. BME680: https://www.bosch-sensortec.com/products/environmental-sensors/gas-sensors/bme680/
6. Source Code: https://gitlab.com/soorajvs13/bme680-stm32-integration

